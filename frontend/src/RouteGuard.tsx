import { useEffect } from 'react';
import { useNavigate, Outlet } from 'react-router-dom';
console.log('helloworld');

export default function RouteGuard() {
	const navigate = useNavigate();

	useEffect(() => {
		const isLogin = true;
		if (!isLogin) {
			navigate('/login');
		}
	}, []);

	return <Outlet />;
}
