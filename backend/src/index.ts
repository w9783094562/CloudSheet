import Koa from 'koa';
import bodyParser from 'koa-bodyparser';
import KoaCORS from '@koa/cors';

import routesInstaller from './routes';

const app = new Koa();

const port: number = 8080;

app.use(bodyParser());

routesInstaller(app);

app.listen(port, () => {
	console.log('listen success', port);
});
