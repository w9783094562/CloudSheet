// router层 路由 路由层只做路由匹配和分发工作
import KoaRouter from '@koa/router';
import { login } from '../controller/userController';
import { UserLoginParams } from '../controller/userController/types';
const userRouter = new KoaRouter();

userRouter.post('/login', async (ctx) => {
	console.log('用户发送登录请求来了', ctx.request.body, ctx.body); // ctx.body === ctx.request.body // 读请求体 ctx.body = ctx.request.body  ctx.response.body

	const user = await login(ctx.request.body as UserLoginParams); // 这段代码报错 用户没查到 异常中间件去的

	if (user) {
		ctx.body = {
			status: 200,
			message: 'ok',
			data: user,
		};
	} else {
		ctx.body = {
			status: 4009,
			message: 'password is not correct',
			data: null,
		};
	}

	// 开始处理 用户登录请求 我们自然的交给 用户登录的controller 去写对应的业务逻辑对不对
	// 因为我们实际上在处理登录的时候 我们是要去读数据库 所以我们要做成异步
	// const result = await userController.login(ctx.body) // ctx.body --> 请求体

	// 分发给controller

	// 匹配对应的controller 文件 userController
});

// koa的洋葱模型 中间件

// restful api

// userRouter.post("/logout")

export default userRouter.routes(); // 这个时候会导出去一个中间件「监听用户请求的函数

// 得益于我们安装了 @types/koa_router这个类型库

// 和用户相关的所有路由

// - 登录
// - 注销
// - 注册
// - 用户的xxx
