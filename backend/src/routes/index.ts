import Koa from 'koa';

import userRouter from './user';

export default function (ctx: Koa<Koa.DefaultState, Koa.DefaultContext>) {
	ctx.use(userRouter);
}
